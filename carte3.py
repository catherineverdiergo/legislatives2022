from pyspark.sql import functions as F
from pyspark.sql.types import *
from pyspark.sql import SparkSession
from pyspark.sql.window import Window


spark = SparkSession \
    .builder \
    .appName("Python Spark carte1") \
    .config("spark.sql.crossJoin.enabled", "true") \
    .getOrCreate()

# read circos2_pqt dataset created with the carte2.py script
co = spark.read.parquet('circos2_pqt')
# use the 2rd round of the presidential election
sr = spark.read.option("delimiter",";").option("header","true").csv("results2.csv")

to_drop=['Etat saisie','% Abs/Ins','% Vot/Ins','% Blancs/Ins','% Blancs/Vot',
         '% Nuls/Ins','% Nuls/Vot','% Exp/Ins','% Exp/Vot']
for i in range(1, 3):
    for f in ['N°Panneau', 'Sexe', 'Voix', '% Voix/Ins', '% Voix/Exp', 'Nom', 'Prénom']:
        to_drop.append("%s%d" % (f, i))
    sr = sr.withColumn(f'% Voix/Exp{i}', F.regexp_replace(F.col(f'% Voix/Exp{i}'), ',', '.'))
    sr = sr.withColumn(f'% Voix/Exp{i}', sr[f'% Voix/Exp{i}'].cast('double'))

# advantage weight for the presidence over RN by district
sr = sr.withColumn('avantage_majp_on_RN', sr['% Voix/Exp1'] - sr['% Voix/Exp2'])

sr = sr.drop(*to_drop)

# fix ultramarine department codes (to be compliant with gejson file)
ULTRAMARINE_DPT_MAP = {
  "ZA": "971",
  "ZB": "972",
  "ZC": "973",
  "ZD": "974",
  "ZM": "976",
  "ZN": "988",
  "ZW": "986",
  "ZS": "975",
  "ZX": "978"
}
udf_fix_ultramarine = F.udf(lambda x: ULTRAMARINE_DPT_MAP[x] if x in ULTRAMARINE_DPT_MAP.keys() else x, StringType())
sr=sr.withColumn("Code du département", udf_fix_ultramarine(F.col('Code du département')))
# REF of district
sr=(sr.withColumn('REF', F.concat(F.lpad(F.col('Code du département'), 3, '0'),
                  F.lit('-'), F.col('Code de la circonscription')))
      .drop('Code du département', 'Code de la circonscription')
    )
# Label of districts
sr=(sr.withColumn('REF_LABEL', F.concat(F.col('Libellé du département'),
                  F.lit(' / '), F.col('Libellé de la circonscription')))
      .drop(' Libellé du département', 'Libellé de la circonscription')
    )

sr = sr.select('REF', 'REF_LABEL', 'avantage_majp_on_RN')

co = co.join(sr, ['REF', 'REF_LABEL'])

qualif=co.filter(co.pc >= 12.5).select('REF','party').withColumn('qualified', F.lit(True))
co = co.join(qualif,['REF','party'],'left_outer')
udf_qualified = F.udf(lambda qualified: qualified if qualified else False, BooleanType())
co=co.withColumn('qualified', udf_qualified(co['qualified']))

# Vote transfers
# Transfers for LO, NPA, DLF
udf_transfer1 = F.udf(lambda party: 'NUPES' if party in ['LO', 'NPA'] else 'RN' if party == 'DLF' else party, StringType())
co = co.withColumn('party', udf_transfer1(co.party)).drop('Voix', 'Exprimés')

# Transfers for Reconquête and LR (rules are more complicated)
def transfer(co, party):
    result = None
    if party not in ['Reconquête', 'LR']:
        result = co
    else:
        transfer = co.filter((co.party == party) & (co.qualified == False))
        if party == 'LR':
            transferRe = transfer.withColumn('pc', transfer.pc * 0.5).withColumn('party', F.lit('Renaissance'))
            transferRn = transfer.withColumn('pc', transfer.pc * 0.5).withColumn('party', F.lit('RN'))
            transfer = transferRn.union(transferRe)
        else:  # party == Reconquête
            rn1 = co.filter((co.party == 'RN') & (co.qualified == True) & (co.avantage_majp_on_RN < 0)).select('REF', 'REF_LABEL')
            rn2 = co.filter((co.party == 'RN') & (co.qualified == True) & (co.avantage_majp_on_RN >= 0)).select('REF', 'REF_LABEL')
            transferRe1 = transfer.join(rn1, ['REF', 'REF_LABEL']).withColumn('pc', transfer.pc * 0.2).withColumn('party', F.lit('Renaissance'))
            transferRn1 = transfer.join(rn1, ['REF', 'REF_LABEL']).withColumn('pc', transfer.pc * 0.8).withColumn('party', F.lit('RN'))
            transferRe2 = transfer.join(rn2, ['REF', 'REF_LABEL']).withColumn('pc', transfer.pc * 0.8).withColumn('party', F.lit('Renaissance'))
            transferRn2 = transfer.join(rn2, ['REF', 'REF_LABEL']).withColumn('pc', transfer.pc * 0.2).withColumn('party', F.lit('RN'))
            transfer = transferRn1.union(transferRe1).union(transferRn2).union(transferRe2)
        result = co.select(*transfer.columns).filter((co.party != party) | (co.qualified == True)).union(transfer)
    return result

co = transfer(co, 'LR')
co = transfer(co, 'Reconquête')
co = co.drop('qualified')
co = co.groupBy('REF', 'REF_LABEL', 'party', 'avantage_majp_on_RN').sum('pc')
co = co.withColumnRenamed('sum(pc)', 'pc')

windowSpec  = Window.partitionBy('REF', 'REF_LABEL', 'avantage_majp_on_RN').orderBy(F.col('pc').desc())
co = co.withColumn('rank', F.rank().over(windowSpec))

co = co.filter(co.rank == 1).orderBy('REF').drop('rank')

co.select('REF', 'REF_LABEL', 'party').groupBy('party').count().show()

# group by and create list of scores field (sorted by 'pc' desc)
#final = (co.groupBy(['REF', 'REF_LABEL'])
#           .agg(F.reverse(F.array_sort(F.collect_list(F.struct(F.col('pc'), F.col('party'))))).alias('result'))
#        )

final = co

# define a color by party
COLORS = {
           "RN": "#336699",
           "NUPES": "#FF6600",
           "Renaissance": "#CC99FF",
           "Reconquête": "#333300",
           "LR": "#6699FF",
         }
# Add color field
#def get_color(scores):
#    return COLORS[scores[0][1]]
def get_color(party):
    return COLORS[party]

udf_color=F.udf(lambda x: get_color(x), StringType())
#final=final.withColumn('fill', udf_color(F.col("result")))
final=final.withColumn('fill', udf_color(F.col("party")))

# load the geojson file
c=spark.read.json('circos.json')
c=c.withColumn('REF', c.properties.REF)
# join the 2 datasets regarding legislative distrinct UID
df_final=final.join(c,'REF')

schema_properties = StructType([
    StructField("REF",StringType(),True),
    StructField("LABEL",StringType(),True),
    StructField("fill",StringType(),True),
    StructField("Parti", StringType(), True),
    StructField("score",DoubleType(),True),
#    StructField("NUPES",DoubleType(),True),
#    StructField("Reconquête", DoubleType(), True),
#    StructField("LR", DoubleType(), True),
  ])

def build_properties(ref, label, fill, party, pc):
    scores = {}
    res = [ref]
    res.append(label)
    res.append(fill)
    res.append(party)
    res.append(pc)
    return res

udf_bp = F.udf(lambda ref, label, fill, party, pc: build_properties(ref, label, fill, party, pc), schema_properties)

df_final = df_final.withColumn('properties', udf_bp(F.col('REF'), F.col('REF_LABEL'), F.col('fill'), F.col('party'), F.col('pc')))
df_final = df_final.drop('REF','REF_LABEL','result', 'fill')

df_final.write.mode('overwrite').parquet('c_aug_carte3_pqt')
df_final=spark.read.parquet('c_aug_carte3_pqt')
df_final.coalesce(1).write.mode('overwrite').json('c_aug_carte3')




