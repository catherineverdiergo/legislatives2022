#!/bin/bash

wget https://static.data.gouv.fr/resources/election-presidentielle-des-10-et-24-avril-2022-resultats-definitifs-du-1er-tour/20220414-152430/resultats-par-niveau-cirlg-t1-france-entiere.txt
iconv -f ISO-8859-1 -t UTF-8 -c resultats-par-niveau-cirlg-t1-france-entiere.txt > res.csv
echo -n "Code du département;Libellé du département;Code de la circonscription;Libellé de la circonscription;" > results.csv
echo -n "Etat saisie;Inscrits;Abstentions;% Abs/Ins;Votants;% Vot/Ins;Blancs;% Blancs/Ins;% Blancs/Vot;Nuls;" >> results.csv
echo -n "% Nuls/Ins;% Nuls/Vot;Exprimés;% Exp/Ins;% Exp/Vot;" >> results.csv
echo -n "N°Panneau1;Sexe1;Nom1;Prénom1;Voix1;% Voix/Ins1;% Voix/Exp1;" >> results.csv
echo -n "N°Panneau2;Sexe2;Nom2;Prénom2;Voix2;% Voix/Ins2;% Voix/Exp2;" >> results.csv
echo -n "N°Panneau3;Sexe3;Nom3;Prénom3;Voix3;% Voix/Ins3;% Voix/Exp3;" >> results.csv
echo -n "N°Panneau4;Sexe4;Nom4;Prénom4;Voix4;% Voix/Ins4;% Voix/Exp4;" >> results.csv
echo -n "N°Panneau5;Sexe5;Nom5;Prénom5;Voix5;% Voix/Ins5;% Voix/Exp5;" >> results.csv
echo -n "N°Panneau6;Sexe6;Nom6;Prénom6;Voix6;% Voix/Ins6;% Voix/Exp6;" >> results.csv
echo -n "N°Panneau7;Sexe7;Nom7;Prénom7;Voix7;% Voix/Ins7;% Voix/Exp7;" >> results.csv
echo -n "N°Panneau8;Sexe8;Nom8;Prénom8;Voix8;% Voix/Ins8;% Voix/Exp8;" >> results.csv
echo -n "N°Panneau9;Sexe9;Nom9;Prénom9;Voix9;% Voix/Ins9;% Voix/Exp9;" >> results.csv
echo -n "N°Panneau10;Sexe10;Nom10;Prénom10;Voix10;% Voix/Ins10;% Voix/Exp10;" >> results.csv
echo -n "N°Panneau11;Sexe11;Nom11;Prénom11;Voix11;% Voix/Ins11;% Voix/Exp11;" >> results.csv
echo "N°Panneau12;Sexe12;Nom12;Prénom12;Voix12;% Voix/Ins12;% Voix/Exp12" >> results.csv
tail -n +2 res.csv >> results.csv
