from pyspark.sql import functions as F
from pyspark.sql.types import *
from pyspark.sql import SparkSession

spark = SparkSession \
    .builder \
    .appName("Python Spark carte1") \
    .getOrCreate()

co=spark.read.option("delimiter",";").option("header","true").csv("results.csv")
# remove unused columns
to_drop=['Etat saisie','% Abs/Ins','% Vot/Ins','% Blancs/Ins','% Blancs/Vot',
         '% Nuls/Ins','% Nuls/Vot','% Exp/Ins','% Exp/Vot']
         
for i in range(1, 13):
    for f in ['N°Panneau', 'Sexe', '% Voix/Ins','% Voix/Exp']:
        to_drop.append("%s%d" % (f, i))

co=co.drop(*to_drop)
# One line per candidate and district
df_bis=None
for i in range(1, 13):
    dftmp = co.select('Code du département','Libellé du département','Code de la circonscription',
                      'Libellé de la circonscription','Inscrits','Abstentions','Blancs','Nuls',
                      'Exprimés','Nom%d' % i, 'Prénom%d' % i, 'Voix%d' % i)
    dftmp = dftmp.withColumnRenamed('Nom%d' % i, 'Nom')
    dftmp = dftmp.withColumnRenamed('Prénom%d' % i, 'Prénom')
    dftmp = dftmp.withColumnRenamed('Voix%d' % i, 'Voix')
    if df_bis is None:
        df_bis = dftmp
    else:
        df_bis = df_bis.union(dftmp)

co = df_bis
to_int_cols = ['Inscrits', 'Abstentions', 'Blancs', 'Nuls', 'Exprimés', 'Voix']
for col in to_int_cols:
    co = co.withColumn(col, co[col].cast('int'))

co=co.withColumn('Candidat', F.concat(co['Prénom'], F.lit(' '), co['Nom'])).drop('Nom', 'Prénom')
# fix ultramarine department codes (to be compliant with gejson file)
ULTRAMARINE_DPT_MAP = {
  "ZA": "971",
  "ZB": "972",
  "ZC": "973",
  "ZD": "974",
  "ZM": "976",
  "ZN": "988",
  "ZW": "986",
  "ZS": "975",
  "ZX": "978"
}
udf_fix_ultramarine = F.udf(lambda x: ULTRAMARINE_DPT_MAP[x] if x in ULTRAMARINE_DPT_MAP.keys() else x, StringType())
co=co.withColumn("Code du département", udf_fix_ultramarine(F.col('Code du département')))
# REF of district
co=(co.withColumn('REF', F.concat(F.lpad(F.col('Code du département'), 3, '0'),
                  F.lit('-'), F.col('Code de la circonscription')))
      .drop('Code du département', 'Code de la circonscription')
    )
# Label of districts
co=(co.withColumn('REF_LABEL', F.concat(F.col('Libellé du département'),
                  F.lit(' / '), F.col('Libellé de la circonscription')))
      .drop(' Libellé du département', 'Libellé de la circonscription')
    )
# percentage by candidate by legislative district
co=co.withColumn('pc', F.col('Voix') / F.col('Exprimés') * F.lit(100.))
# keep a set of fields
co = co.select('REF', 'REF_LABEL', 'Candidat', 'pc')
# group by and create list of scores field (sorted by 'pc' desc)
final = (co.groupBy(['REF', 'REF_LABEL'])
           .agg(F.reverse(F.array_sort(F.collect_list(F.struct(F.col('pc'), F.col('Candidat'))))).alias('result'))
        )
# define a color by candidate
COLORS = {
           "Marine LE PEN": "#336699",
           "Jean-Luc MÉLENCHON": "#FF6600",
           "Emmanuel MACRON": "#CC99FF",
           "Fabien ROUSSEL": "#FF5050",
           "Éric ZEMMOUR": "#333300",
           "Yannick JADOT": "#009900",
           "Valérie PÉCRESSE": "#6699FF",
           "Jean LASSALLE: "#669999",
           "Nathalie ARTHAUD": "#660033",
           "Philippe POUTOU": "#993366",
           "Anne HIDALGO": "#FF6699",
           "Nicolas DUPONT-AIGNAN": "#0066FF",
         }
# Add color field
def get_color(scores):
    return COLORS[scores[0][1]]

udf_color=F.udf(lambda x: get_color(x), StringType())
final=final.withColumn('fill', udf_color(F.col("result")))
# load the geojson file
c=spark.read.json('circos.json')
c=c.withColumn('REF', c.properties.REF)
# join the 2 datasets regarding legislative distrinct UID
df_final=final.join(c,'REF')

schema_properties = StructType([
    StructField("REF",StringType(),True),
    StructField("LABEL",StringType(),True),
    StructField("fill",StringType(),True),
    StructField("Jean-Luc MÉLENCHON",DoubleType(),True),
    StructField("Philippe POUTOU",DoubleType(),True),
    StructField("Éric ZEMMOUR", DoubleType(), True),
    StructField("Anne HIDALGO", DoubleType(), True),
    StructField("Fabien ROUSSEL", DoubleType(), True),
    StructField("Yannick JADOT", DoubleType(), True),
    StructField("Nathalie ARTHAUD", DoubleType(), True),
    StructField("Emmanuel MACRON", DoubleType(), True),
    StructField("Jean LASSALLE", DoubleType(), True),
    StructField("Marine LE PEN", DoubleType(), True),
    StructField("Valérie PÉCRESSE", DoubleType(), True)
  ])

def build_properties(ref, label, fill, results):
    scores = {}
    for sc in results:
        scores[sc[1]] = sc[0]
    res = [ref]
    res.append(label)
    res.append(fill)
    res.append(scores['Jean-Luc MÉLENCHON'])
    res.append(scores['Philippe POUTOU'])
    res.append(scores['Éric ZEMMOUR'])
    res.append(scores['Anne HIDALGO'])
    res.append(scores['Fabien ROUSSEL'])
    res.append(scores['Yannick JADOT'])
    res.append(scores['Nathalie ARTHAUD'])
    res.append(scores['Emmanuel MACRON'])
    res.append(scores['Jean LASSALLE'])
    res.append(scores['Marine LE PEN'])
    res.append(scores['Valérie PÉCRESSE'])
    return res

udf_bp = F.udf(lambda ref, label, fill, results: build_properties(ref, label, fill, results), schema_properties)

df_final = df_final.withColumn('properties', udf_bp(F.col('REF'), F.col('REF_LABEL'), F.col('fill'), F.col('result')))
df_final = df_final.drop('REF','REF_LABEL','result', 'fill')

df_final.coalesce(1).write.mode('overwrite').json('c_aug')

