#!/bin/bash

wget https://static.data.gouv.fr/resources/election-presidentielle-des-10-et-24-avril-2022-resultats-definitifs-du-2nd-tour/20220428-142204/resultats-par-niveau-cirlg-t2-france-entiere.txt
iconv -f ISO-8859-1 -t UTF-8 -c resultats-par-niveau-cirlg-t2-france-entiere.txt > res.csv
echo -n "Code du département;Libellé du département;Code de la circonscription;Libellé de la circonscription;" > results2.csv
echo -n "Etat saisie;Inscrits;Abstentions;% Abs/Ins;Votants;% Vot/Ins;Blancs;% Blancs/Ins;% Blancs/Vot;Nuls;" >> results2.csv
echo -n "% Nuls/Ins;% Nuls/Vot;Exprimés;% Exp/Ins;% Exp/Vot;" >> results2.csv
echo -n "N°Panneau1;Sexe1;Nom1;Prénom1;Voix1;% Voix/Ins1;% Voix/Exp1;" >> results2.csv
echo "N°Panneau2;Sexe2;Nom2;Prénom2;Voix2;% Voix/Ins2;% Voix/Exp2" >> results2.csv
tail -n +2 res.csv >> results2.csv
