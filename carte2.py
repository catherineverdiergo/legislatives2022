from pyspark.sql import functions as F
from pyspark.sql.types import *
from pyspark.sql import SparkSession

spark = SparkSession \
    .builder \
    .appName("Python Spark carte1") \
    .getOrCreate()

co=spark.read.option("delimiter",";").option("header","true").csv("results.csv")
# remove unused columns
to_drop=['Etat saisie','% Abs/Ins','% Vot/Ins','% Blancs/Ins','% Blancs/Vot',
                '% Nuls/Ins','% Nuls/Vot','% Exp/Ins','% Exp/Vot','N°Panneau']
for i, f in enumerate(['N°Panneau', 'Sexe', '% Voix/Ins','% Voix/Exp']):
    to_drop.append("%s%d" % (f, i))

co=co.drop(*to_drop)
# One line per candidate and distrinct
df_bis=None
for i in range(1, 13):
    dftmp = co.select('Code du département','Libellé du département','Code de la circonscription',
                      'Libellé de la circonscription','Inscrits','Abstentions','Blancs','Nuls',
                      'Exprimés','Nom%d' % i, 'Prénom%d' % i, 'Voix%d' % i)
    dftmp = dftmp.withColumnRenamed('Nom%d' % i, 'Nom')
    dftmp = dftmp.withColumnRenamed('Prénom%d' % i, 'Prénom')
    dftmp = dftmp.withColumnRenamed('Voix%d' % i, 'Voix')
    if df_bis is None:
        df_bis = dftmp
    else:
        df_bis = df_bis.union(dftmp)

co = df_bis
to_int_cols = ['Inscrits', 'Abstentions', 'Blancs', 'Nuls', 'Exprimés', 'Voix']
for col in to_int_cols:
    co = co.withColumn(col, co[col].cast('int'))

co=co.withColumn('Candidat', F.concat(co['Prénom'], F.lit(' '), co['Nom'])).drop('Nom', 'Prénom')
# fix ultramarine department codes (to be compliant with gejson file)
ULTRAMARINE_DPT_MAP = {
  "ZA": "971",
  "ZB": "972",
  "ZC": "973",
  "ZD": "974",
  "ZM": "976",
  "ZN": "988",
  "ZW": "986",
  "ZS": "975",
  "ZX": "978"
}
udf_fix_ultramarine = F.udf(lambda x: ULTRAMARINE_DPT_MAP[x] if x in ULTRAMARINE_DPT_MAP.keys() else x, StringType())
co=co.withColumn("Code du département", udf_fix_ultramarine(F.col('Code du département')))
# REF of district
co=(co.withColumn('REF', F.concat(F.lpad(F.col('Code du département'), 3, '0'),
                  F.lit('-'), F.col('Code de la circonscription')))
      .drop('Code du département', 'Code de la circonscription')
    )
# Label of districts
co=(co.withColumn('REF_LABEL', F.concat(F.col('Libellé du département'),
                  F.lit(' / '), F.col('Libellé de la circonscription')))
      .drop(' Libellé du département', 'Libellé de la circonscription')
    )
co = co.select('REF', 'REF_LABEL', 'Candidat', 'Voix')
# Alliances
ALLIANCES = {
           "Marine LE PEN": [("RN", 1.)],
           "Jean-Luc MÉLENCHON": [("NUPES", 1.)],
           "Emmanuel MACRON": [("Renaissance", 1.)],
           "Fabien ROUSSEL": [("NUPES", 1.)],
           "Éric ZEMMOUR": [("Reconquête", 1.)],
           "Yannick JADOT": [("NUPES", 0.75), ("Renaissance", 0.15)],
           "Valérie PÉCRESSE": [("LR", 1.)],
           "Jean LASSALLE": [],
           "Nathalie ARTHAUD": [("LO", 1.)],
           "Philippe POUTOU": [("NPA", 1.)],
           "Anne HIDALGO": [("NUPES", 0.75), ("Renaissance", 0.15)],
           "Nicolas DUPONT-AIGNAN": [("DLF", 1.)],
}

schema_alliances = StructType([
    StructField("party",StringType(),True),
    StructField("transfer",DoubleType(),True)
  ])

udf_party=F.udf(lambda candidat: ALLIANCES[candidat], ArrayType(schema_alliances))

co = co.withColumn("alliances", udf_party(co.Candidat))

co = (co.select('REF','REF_LABEL','Candidat','Voix',F.explode(co.alliances).alias('alliance'))
        .select('REF','REF_LABEL','Candidat','Voix', F.col('alliance').party.alias('party'), F.col('alliance').transfer.alias('transfer'))
     )

# Penalties
PENALTIES = {
           "Marine LE PEN": 0.87,
           "Jean-Luc MÉLENCHON": 0.95,
           "Emmanuel MACRON": 1.,
           "Fabien ROUSSEL": 0.95,
           "Éric ZEMMOUR": 1.,
           "Yannick JADOT": 1.,
           "Valérie PÉCRESSE": 1.,
           "Jean LASSALLE": 0,
           "Nathalie ARTHAUD": 1.,
           "Philippe POUTOU": 1.,
           "Anne HIDALGO": 0.9,
           "Nicolas DUPONT-AIGNAN": 1.,
}

udf_penalty=F.udf(lambda candidat: PENALTIES[candidat], DoubleType())

co = co.withColumn('penalty', udf_penalty(co.Candidat))

co = co.withColumn('Voix_Corr', F.round(co.Voix*co.transfer*co.penalty).cast('integer'))

co = (co.select('REF', 'REF_LABEL', 'party', 'Voix_Corr')
        .groupBy('REF', 'REF_LABEL', 'party').sum('Voix_Corr')
        .groupBy('REF', 'REF_LABEL', 'party').sum('sum(Voix_Corr)')
        .withColumnRenamed('sum(sum(Voix_Corr))', 'Voix')
     )

# Compute new Exprimé
expr = co.select('REF', 'REF_LABEL', 'Voix').groupBy('REF', 'REF_LABEL').sum('Voix').withColumnRenamed('sum(Voix)', 'Exprimés')
co=co.join(expr, ['REF', 'REF_LABEL'])

# percentage by candidate by legislative district
co=co.withColumn('pc', F.col('Voix') / F.col('Exprimés') * F.lit(100.))
# save for dataset for next map (carte3)
co.write.mode('overwrite').parquet('circos2_pqt')
# keep a set of fields
co = co.select('REF', 'REF_LABEL', 'party', 'pc')
# get qualified by district
qualif=co.filter(co.pc >= 12.5)
# group by and create list of scores field (sorted by 'pc' desc)
final_qualif = (qualif.groupBy(['REF', 'REF_LABEL'])
                  .agg(F.reverse(F.array_sort(F.collect_list(F.struct(F.col('pc'), F.col('party'))))).alias('qualif'))
        )
# group by and create list of scores field (sorted by 'pc' desc)
final = (co.groupBy(['REF', 'REF_LABEL'])
           .agg(F.reverse(F.array_sort(F.collect_list(F.struct(F.col('pc'), F.col('party'))))).alias('result'))
        )
final = final.join(final_qualif, ['REF', 'REF_LABEL'])
# define a color by party
COLORS = {
           "RN": "#336699",
           "NUPES": "#FF6600",
           "Renaissance": "#CC99FF",
           "Reconquête": "#333300",
           "LR": "#6699FF",
           "LO": "#660033",
           "NPA": "#993366",
           "DLF": "#0066FF"
         }
# Add color field
def get_color(scores):
    return COLORS[scores[0][1]]

udf_color=F.udf(lambda x: get_color(x), StringType())
final=final.withColumn('fill', udf_color(F.col("result")))
# load the geojson file
c=spark.read.json('circos.json')
c=c.withColumn('REF', c.properties.REF)
# join the 2 datasets regarding legislative distrinct UID
df_final=final.join(c,'REF')

schema_properties = StructType([
    StructField("REF",StringType(),True),
    StructField("LABEL",StringType(),True),
    StructField("Qualifiès",StringType(),True),
    StructField("fill",StringType(),True),
    StructField("Renaissance", DoubleType(), True),
    StructField("RN",DoubleType(),True),
    StructField("NUPES",DoubleType(),True),
    StructField("Reconquête", DoubleType(), True),
    StructField("LR", DoubleType(), True),
    StructField("DLF", DoubleType(), True),
    StructField("NPA", DoubleType(), True),
    StructField("LO", DoubleType(), True)
  ])

def build_properties(ref, label, fill, results, qualif):
    scores = {}
    for sc in results:
        scores[sc[1]] = sc[0]
    res = [ref]
    res.append(label)
    qualif_str = ""
    for i,q in enumerate(qualif):
        qualif_str+=f"{q[1]}({q[0]})"
        if i < len(qualif)-1:
          qualif_str += ","
    res.append(qualif_str)
    res.append(fill)
    res.append(scores['Renaissance'])
    res.append(scores['RN'])
    res.append(scores['NUPES'])
    res.append(scores['Reconquête'])
    res.append(scores['LR'])
    res.append(scores['DLF'])
    res.append(scores['NPA'])
    res.append(scores['LO'])
    return res

udf_bp = F.udf(lambda ref, label, fill, results, qualif: build_properties(ref, label, fill, results, qualif), schema_properties)

df_final = df_final.withColumn('properties', udf_bp(F.col('REF'), F.col('REF_LABEL'), F.col('fill'), F.col('result'), F.col('qualif')))
df_final = df_final.drop('REF','REF_LABEL','result', 'fill', 'qualif')

df_final.write.mode('overwrite').parquet('c_aug_carte2_pqt')
df_final=spark.read.parquet('c_aug_carte2_pqt')
df_final.coalesce(1).write.mode('overwrite').json('c_aug_carte2')

